This project is no longer maintained.

In fact, it didn't ever exist; it was created with a .gitreview
and that's it.

The intent was for it to be used for configuring NFS Ganesha server,
but ceph-ansible will install/configure NFS Ganesha server, so this
wasn't ever used.
